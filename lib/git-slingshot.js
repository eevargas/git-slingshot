const {
  info,
  cls,
  gcb,
  getDirs,
  isGitRepo,
  drawLine
} = require('./lib')

const sync = require('./git-sync')

module.exports = function (patterns, { remote }) {
  cls()
  let childDirs = patterns.length
    ? patterns.reduce((acc, pattern) => [ ...acc, ...getDirs(pattern) ], [])
    : getDirs()

  drawLine()

  childDirs.forEach(([ repoPath, repoName ]) => {
    process.chdir(repoPath)
    if (!isGitRepo()) { return }

    info('Syncing', `${repoName}:${gcb()}`)
    sync(remote)
    drawLine()
  })
}
