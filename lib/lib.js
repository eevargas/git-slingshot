const { readdirSync, lstatSync } = require('fs')
const { join } = require('path')
const { execSync } = require('child_process')
const {
  gray,
  yellow,
  blue,
  red
} = require('chalk')

/**
 * Pretty print data to the console
 * @param  {any} args
 */
function dd (data) {
  console.log(JSON.stringify(data, null, 2))
}

/**
 * Pretty print message or data to the console
 * @param  {...string} args
 */
function log (msg, prefix = ' * ') {
  console.log(`${prefix} ${msg}`)
}

/**
 * Print a pre-fixed/stringified message to the console
 * @param {string} msg
 * @param {string} details
 */
function warn (msg, details) {
  console.log(
    `${yellow(`[WARN] ${msg}`)}${details ? ': ' + gray(details) : ''}`
  )
}

/**
 * Print a pre-fixed/stringified message to the console
 * @param {string} msg
 * @param {string} details
 */
function info (msg, details) {
  console.log(
    `${blue(`[INFO] ${msg}`)}${details ? ': ' + yellow(details) : ''}`
  )
}

/**
 * Print a pre-fixed/stringified message to the console
 * @param {string} msg
 * @param {string} details
 */
function err (msg, details) {
  console.log(
    `${red.bold(`[ERROR] ${msg}`)}${details ? ': ' + yellow(details) : ''}`
  )
}

function cls () {
  execSync('clear', { stdio: 'inherit' })
}

function drawLine () {
  const cols = execSync('tput cols', { encoding: 'utf8' }).trim()
  console.log(gray.bold('-'.repeat(cols)))
}

function gcb () {
  return execSync(
    'git rev-parse --symbolic-full-name --abbrev-ref HEAD',
    { encoding: 'utf8' }
  ).trim()
}

function isDirty () {
  return !!execSync(
    'git status -s',
    { encoding: 'utf8' }
  ).trim()
}

function parseBranchNames(input, remote=false) {
  return input.trim().split('\n').reduce((acc, itm) => {
    let out = itm.trim()

    if (out.indexOf('HEAD') !== -1) { return acc }

    if (remote) { out = out.replace(/\w*\//, '') }

    acc.push(out.replace('* ', ''))
    return acc
  }, [])
}

function listBranches () {
  const remote = execSync('git branch -r', { encoding: 'utf8' })
  const local = execSync('git branch -l', { encoding: 'utf8' })

  return new Set([
    ...parseBranchNames(remote, true),
    ...parseBranchNames(local)
  ])
}

function git (command) {
  let out = { error: false, output: null }
  try {
    out.output = execSync(`git ${command}`, { stdio: 'pipe', encoding: 'utf8' })
  } catch (err) {
    out.error = true
    out.output = err.stdout
  }

  return out
}

function isGitRepo () {
  return !!getDirs('^.git$').length
}

const PROTECTED_BRANCHES = process.env.GIT_PROTECTED_BRANCHES
  ? [ '^*', ...process.env.GIT_PROTECTED_BRANCHES.replace(/\s+/, '').split(',') ]
  : [ '^*', 'develop', 'master', 'main' ]

function rmMerged () {
  let branches

  try {
    branches = execSync(
      `git branch --merged | grep -v ${PROTECTED_BRANCHES.join(' | grep -v ')}`,
      { encoding: 'utf8' }
    ).replace(/\n/, '').trim()
    return git(`branch -d ${branches}`)
  } catch (err) {
    return { error: true, output: 'No merged branches to remove!' }
  }
}

/**
 * Find immediate child directories, optionally filter by regex
 * @param {string} pattern regex string to filter the returned dir list
 */
function getDirs (pattern) {
  const source = process.cwd()
  return readdirSync(source)
    .filter((itm) => {
      if (!pattern) { return true }
      const regEx = new RegExp(pattern, 'g')
      return (itm.search(regEx) !== -1)
    })
    .map((itm) => [ join(source, itm), itm ])
    .filter((itm) => lstatSync(itm[0]).isDirectory())
}

module.exports = {
  cls,
  dd,
  drawLine,
  err,
  gcb,
  getDirs,
  git,
  info,
  isDirty,
  isGitRepo,
  listBranches,
  log,
  rmMerged,
  warn
}
