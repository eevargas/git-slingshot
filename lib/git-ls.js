const {
  log,
  info,
  cls,
  gcb,
  getDirs,
  isDirty,
  isGitRepo,
  drawLine
} = require('./lib')

const { yellow, blue, red } = require('chalk')

module.exports = function (patterns, { exclude, match }) {
  cls()

  let childDirs = patterns.length
    ? patterns.reduce((acc, pattern) => [ ...acc, ...getDirs(pattern) ], [])
    : getDirs()

  drawLine()
  if (exclude) {
    info('Excluding repos on branch(es)', exclude)
    exclude = (exclude.indexOf(',') !== -1) ? exclude.replace(/\s+/, '').split(',') : [ exclude ]
    drawLine()
  }

  if (match) {
    info('Matching repos on branch(es)', match)
    match = (match.indexOf(',') !== -1) ? match.replace(/\s+/, '').split(',') : [ match ]
    drawLine()
  }

  childDirs.forEach(([ repoPath, repoName ]) => {
    process.chdir(repoPath)
    if (!isGitRepo()) { return }
    const branchName = gcb()

    if (exclude && exclude.includes(branchName)) { return }

    if (match && !match.includes(branchName)) { return }

    log(`${blue.bold(repoName)} is on ${yellow.bold(branchName)} ${isDirty() ? red.bold('*DIRTY*') : ''}`)
  })

  drawLine()
}
