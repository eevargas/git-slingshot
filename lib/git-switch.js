const {
  cls,
  drawLine,
  err,
  gcb,
  getDirs,
  git,
  info,
  isDirty,
  isGitRepo,
  listBranches,
  log,
  warn
} = require('./lib')

const { yellow, blue, red } = require('chalk')

module.exports = function (args, { exclude, match, dry }) {
  cls()

  const nextBranch = args.shift()

  if (!nextBranch) {
    console.log()
    err('Missing argument', 'Argument <nextBranch> is require.')
    console.log()
    process.exit(1)
  }

  let childDirs = args.length
    ? args.reduce((acc, pattern) => [ ...acc, ...getDirs(pattern) ], [])
    : getDirs()

  drawLine()

  if (dry) {
    info('Dry run', 'Changes will not be applied.')
    drawLine()
  }

  if (exclude) {
    info('Excluding repos on branch(es)', exclude)
    exclude = (exclude.indexOf(',') !== -1) ? exclude.replace(/\s+/, '').split(',') : [ exclude ]
    drawLine()
  }

  if (match) {
    info('Matching repos on branch(es)', match)
    match = (match.indexOf(',') !== -1) ? match.replace(/\s+/, '').split(',') : [ match ]
    drawLine()
  }

  childDirs.forEach(([ repoPath, repoName ]) => {
    process.chdir(repoPath)

    if (!isGitRepo()) { return }

    const curBranchName = gcb()
    const isNewBranch = !listBranches().has(nextBranch)

    if (exclude && exclude.includes(curBranchName)) { return }

    if (match && !match.includes(curBranchName)) { return }

    if (curBranchName === nextBranch) {
      log(`${blue.bold(repoName)} is already on ${yellow.bold(nextBranch)} ${isDirty() ? red.bold('*DIRTY*') : ''}`)
      drawLine()
      return
    }

    log(`${blue.bold(repoName)} is on ${yellow.bold(curBranchName)}, ${isNewBranch ? 'creating branch' : 'switching to'} ${yellow.bold(nextBranch)}`)
    drawLine()

    if (isDirty()) {
      warn('Dirty Repo', 'This repo has uncommitted or un-tracked files.')
      info('This repo will be skipped! Stash or commit your changes, then try again.')
      drawLine()
      return
    }

    if (dry) { return }

    git('fetch -p')
    git('add -A')
    const checkoutResult = git(`checkout ${isNewBranch ? '-b' : ''} ${nextBranch}`)

    if (checkoutResult.error) {
      err('Checkout', checkoutResult.output)
    } else {
      log(checkoutResult.output)
    }

    drawLine()
  })
}
