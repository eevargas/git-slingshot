const {
  rmMerged,
  warn,
  info,
  drawLine,
  isGitRepo
} = require('./lib')

module.exports = function () {
  if (!isGitRepo()) {
    warn('Not a git repo... skipping!')
    return
  }

  drawLine()
  info('Removing merged branches...')
  console.log()
  rmMerged()
  drawLine()
}
