const {
  gcb,
  git,
  info,
  isDirty,
  isGitRepo,
  log,
  rmMerged,
  warn
} = require('./lib')

const os = require('os')
const fs = require('fs')
const path = require('path')
const { yellow } = require('chalk')

module.exports = function (remote, push = false, rmLocal = false) {
  if (!isGitRepo()) {
    warn('Not a git repo... skipping!')
    return
  }
  const curBranch = gcb()
  const needStashing = isDirty()

  git('fetch -p')

  if (needStashing) {
    info('Repo is dirty... stashing changes...')
    git('add -A')
    git('stash')
  }

  console.log()
  info(`Pulling ${yellow.bold(`${remote}/${curBranch}`)}`)
  const pullResult = git(`pull ${remote} ${curBranch}`)

  console.log()
  log(pullResult.output)

  if (pullResult.error) {
    if (pullResult.output.indexOf('CONFLICT') !== -1) {
      const tmpFile = `${os.tmpdir()}/git-sync.log`

      warn('There where conflicts while pulling', `Output was save to ${tmpFile}`)

      fs.appendFileSync(
        tmpFile,
        `\n[${new Date().toLocaleString()}] ${path.basename(process.cwd())} (${remote}/${curBranch}):\n${pullResult.output}`
      )
    }
    return
  }

  if (push) {
    info(`Pushing ${yellow.bold(`${remote}/${curBranch}`)}`)
    console.log()
    git(`push ${remote} ${curBranch}`)
  }

  if (needStashing) {
    info('Un-stashing changes...')
    console.log()
    git('stash pop')
  }

  if (rmLocal) {
    const rmResult = rmMerged()
    if (rmResult) {
      info('Removing Branches')
      log(rmResult.output, rmResult.error ? ' * ' : '')
    }
  }
}
