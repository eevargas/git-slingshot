# git-slingshot

A collection of git helpers to manipulate multiple repos under a directory.

![screenshot](screenshots/demo.gif)

## git-slingshot

```txt
Usage: git-slingshot [options] [pattern...]

Look for git repos under current directory and fetch, stash, pull and un-stash each of them.

Note: pattern can be a dir-name or a regex (use quote around regex).

Options:
  -V, --version          output the version number
  -r, --remote <remote>  Remote repo other than origin (default: "origin")
  -h, --help             display help for command
```

## git-ls

```txt
Usage: git-ls [options] [pattern...]

Look for git repos under current directory and list the current branch.

Arguments:
  pattern                 (Optional) Repository name, partial name or regex expression.

Options:
  -V, --version           output the version number
  -x, --exclude <branch>  Exclude repositories currently in branch name
  -m, --match <branch>    Include repositories that matches branch name
  -h, --help              display help for command
```

## git-switch

```
Usage: git-switch [options] <nextBranch> [pattern...]

Look for git repos under current directory and switch the current branch.

Arguments:
  nextBranch              Name of an existing branch or a new branch to be created
  pattern                 (Optional) Repository name, partial name or regex expression.

Options:
  -V, --version           output the version number
  -x, --exclude <branch>  Exclude repositories currently in branch name
  -m, --match <branch>    Include repositories that matches branch name
  -d, --dry               Dry run.
  -h, --help              display help for command
```


## git-sync

```txt
Usage: git-sync [options]

Fetch, stash, pull, un-stash and run git-rm-merged on the current repo

Options:
  -V, --version          output the version number
  -r, --remote <remote>  Remote repo other than origin (default: "origin")
  -p, --push             Push local commits (default: "false")
  -h, --help             display help for command
```

## git-rm-merged

```txt
Usage: git-rm-merged [options]

Remove merged branch except for "protected branches" and current branch. By default "protected branches" are  `master`, `develop`, `main` and will not be removed. To overwrite the list of protected branches export and env var `GIT_PROTECTED_BRANCHES` as a csv list of branches in your .bashrc or .bash_profile.

e.g. export GIT_PROTECTED_BRANCHES=master,release,develop,testing

Options:
  -V, --version  output the version number
  -h, --help     display help for command
```
