#!/usr/bin/env node

const program = require('commander')
const slingshot = require('../lib/git-slingshot')

program
  .version(require('../package.json').version)
  .description(
    'Look for git repos under current directory and fetch, stash, pull and un-stash ' +
    'each of them.' +
    '\n\n' +
    'Note: pattern can be a dir-name or a regex (use quote around regex).'
  )
  .arguments('[pattern...]')
  .option('-r, --remote <remote>', 'Remote repo other than origin', 'origin')
  .parse(process.argv)

slingshot(program.args, program.opts())

process.exit(0)
