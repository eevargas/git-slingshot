#!/usr/bin/env node

const program = require('commander')
const sync = require('../lib/git-sync')

program
  .version(require('../package.json').version)
  .description('Fetch, stash, pull, un-stash and run git-rm-merged on the current repo')
  .option('-r, --remote <remote>', 'Remote repo other than origin', 'origin')
  .option('-p, --push', 'Push local commits')
  .parse(process.argv)

const { remote, push = false } = program.opts()

sync(remote, push, true)

process.exit(0)
