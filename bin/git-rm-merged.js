#!/usr/bin/env node

const program = require('commander')
const rmMerged = require('../lib/git-rm-merged')

program
  .version(require('../package.json').version)
  .description(
    'Remove merged branch except for "protected branches" and current branch. ' +
    'By default "protected branches" are  `master`, `develop`, `main` ' +
    'and will not be removed. To overwrite the list of protected branches ' +
    'export and env var `GIT_PROTECTED_BRANCHES` as a csv list of branches in ' +
    'your .bashrc or .bash_profile. ' +
    '\n\n' +
    'e.g. export GIT_PROTECTED_BRANCHES=master,release,develop,testing'
  )
  .parse(process.argv)

rmMerged()

process.exit(0)
