#!/usr/bin/env node

const program = require('commander')
const ls = require('../lib/git-ls')

program
  .version(require('../package.json').version)
  .description(
    'Look for git repos under current directory and list the current branch. ',
    {
      pattern: '(Optional) Repository name, partial name or regex expression.'
    }
  )
  .arguments('[pattern...]')
  .option('-x, --exclude <branch>', 'Exclude repositories currently in branch name')
  .option('-m, --match <branch>', 'Include repositories that matches branch name')
  .parse(process.argv)

ls(program.args, program.opts())

process.exit(0)
