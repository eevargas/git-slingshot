#!/usr/bin/env node

const program = require('commander')
const gitSwitch = require('../lib/git-switch')

program
  .version(require('../package.json').version)
  .description(
    'Look for git repos under current directory and switch the current branch. ',
    {
      nextBranch: 'Name of an existing branch or a new branch to be created',
      pattern: '(Optional) Repository name, partial name or regex expression.'
    }
  )
  .arguments('<nextBranch> [pattern...]')
  .option('-x, --exclude <branch>', 'Exclude repositories currently in branch name')
  .option('-m, --match <branch>', 'Include repositories that matches branch name')
  .option('-d, --dry', 'Dry run.')
  .parse(process.argv)

gitSwitch(program.args, program.opts())

process.exit(0)
